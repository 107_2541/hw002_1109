package edu.cyut.javaclass;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        String phones1 = "阿公 的手機號碼：0939-100391\n"
                + "阿嬤 的手機號碼：0939-666888";
        String phones2 = "老公 的手機號碼：0952-600391\n"
                + "老婆 的手機號碼：0939-550391";

        Pattern pattern = Pattern.compile(".*0939-\\d{6}");
        String[] result = pattern.split(phones1);
        //Matcher matcher = pattern.split(phones1);
        for (String s : result)
            System.out.println(s);
        result = pattern.split(phones2);
        for (String s : result)
            System.out.println(s);
    }
}